#![no_std]
#![no_main]

use core::cell::RefCell;
use core::fmt::Write;
use core::ops::DerefMut;

// pick a panicking behavior
use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics

extern crate cortex_m;
extern crate cortex_m_rt as rt;
extern crate stm32l4xx_hal as hal;

use crate::hal::delay::Delay;
use crate::hal::pac::interrupt;
use crate::hal::prelude::*;
use crate::hal::serial::Serial;
use crate::rt::entry;
use crate::rt::exception;
use crate::rt::ExceptionFrame;
use cortex_m::interrupt::Mutex;
use cortex_m::peripheral::NVIC;

// Set up global state. It's all mutexed up for concurrency safety.
static G_BUTTON: Mutex<RefCell<Option<hal::gpio::PC13<hal::gpio::Input<hal::gpio::PullUp>>>>> =
    Mutex::new(RefCell::new(None));

// Make LED pin globally available
static G_LED: Mutex<RefCell<Option<hal::gpio::PB13<hal::gpio::Output<hal::gpio::PushPull>>>>> =
    Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let mut dp = hal::stm32::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain(); // .constrain();
    let mut rcc = dp.RCC.constrain();
    let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

    let clocks = rcc.cfgr.sysclk(80.MHz()).freeze(&mut flash.acr, &mut pwr);

    let mut gpioa = dp.GPIOA.split(&mut rcc.ahb2);
    let mut gpiob = dp.GPIOB.split(&mut rcc.ahb2);
    let mut gpioc = dp.GPIOC.split(&mut rcc.ahb2);

    // Configure LED
    let mut led = gpiob
        .pb13
        .into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);
    led.set_high();

    // Configure serial
    let tx = gpioa
        .pa2
        .into_alternate(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);
    let rx = gpioa
        .pa3
        .into_alternate(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);

    // TRY using a different USART peripheral here
    let serial = Serial::usart2(dp.USART2, (tx, rx), 115_200.bps(), clocks, &mut rcc.apb1r1);
    let (mut tx, _) = serial.split();

    // Configure button
    let mut board_btn = gpioc
        .pc13
        .into_pull_up_input(&mut gpioc.moder, &mut gpioc.pupdr);
    board_btn.make_interrupt_source(&mut dp.SYSCFG, &mut rcc.apb2);
    board_btn.enable_interrupt(&mut dp.EXTI);
    board_btn.trigger_on_edge(&mut dp.EXTI, hal::gpio::Edge::Falling);

    // Enable interrupts
    unsafe {
        NVIC::unmask(hal::stm32::Interrupt::EXTI15_10);
    }

    // cortex_m::interrupt::free(|cs| *G_BUTTON.borrow(cs).borrow_mut() = Some(board_btn));
    cortex_m::interrupt::free(|cs| {
        G_BUTTON.borrow(cs).replace(Some(board_btn));
    });

    // Move the pin into our global storage
    // cortex_m::interrupt::free(|cs| *G_LED.borrow(cs).borrow_mut() = Some(led));
    cortex_m::interrupt::free(|cs| {
        G_LED.borrow(cs).replace(Some(led));
    });

    // Test serial
    let log = "Hello World\r\n";
    tx.write_str(log).unwrap();

    // let mut timer = Delay::new(cp.SYST, clocks);

    // Test LED
    loop {
        // // block!(timer.wait()).unwrap();
        // timer.delay_ms(1000_u32);
        // led.set_high();

        // // block!(timer.wait()).unwrap();
        // timer.delay_ms(1000_u32);
        // led.set_low();
    }
}

#[interrupt]
fn EXTI15_10() {
    cortex_m::interrupt::free(|cs| {
        let mut led_ref = G_LED.borrow(cs).borrow_mut();
        if let Some(ref mut led) = led_ref.deref_mut() {
            led.toggle();
        }

        let mut btn_ref = G_BUTTON.borrow(cs).borrow_mut();
        if let Some(ref mut btn) = btn_ref.deref_mut() {
            if btn.check_interrupt() {
                // if we don't clear this bit, the ISR would trigger indefinitely
                btn.clear_interrupt_pending_bit();
            }
        }
    });
}

#[exception]
unsafe fn HardFault(ef: &ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}
